# py-script

#### 介绍
一些python脚本, windows建议安装cmder和git

python下载地址：https://www.python.org/downloads

python菜鸟教程：https://www.runoob.com/python3/python3-install.html

cmder下载地址：https://cmder.net

git下载地址：https://git-scm.com/downloads

代码仓库地址：https://gitee.com/wekidsbest/py-script.git

#### 软件架构
python版本：python3

#### 安装教程

#### 软件包安装： 
python -m pip install openpyxl

python -m pip install pandas

python -m pip install xlwings


#### 执行： 
python grab-sheet.py


### 使用说明
grab_sheet.py -- 从大excel中抽取部分sheets

extract_col.py -- 从source表里抽取指定列替换掉target表里的对应列
