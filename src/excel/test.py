from src.excel.work_book import WorkBook

sourceFile = './resources/source/extract/source.xlsx'
sheetName = '现货'

if __name__ == '__main__':
	wb = WorkBook(sourceFile)
	ws = wb.get_sheet(sheetName)

	print('sheet_name:', ws.get_sheet_name())
	print('最大行列：', ws.get_max_row(), ws.get_max_col())
	print('B1:', ws.get_cell_value(1, 2), ws.get_cell_value_by_name('B1'))
	print('A1:A5', ws.get_ws().range((1, 1), (1, 5)).value)
	print('第一行', ws.get_first_row_value())
	print('第一列', ws.get_col_list('作价始日').value)

	ws.replace_col_inner('运杂费', '作价始日')

	print("代码执行完毕！")
