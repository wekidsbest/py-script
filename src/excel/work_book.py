#!/usr/bin/env python3

import os
import xlwings as xw
from .sheet import Sheet


# 工作簿类
class WorkBook:
	def __init__(self, file_path):
		if not os.path.exists(file_path):
			raise FileNotFoundError(file_path)
		print('[Running] 正在加载excel文件:【', file_path, '】...')
		self.app = xw.App(visible=True, add_book=False)
		# 不显示Excel消息框
		self.app.display_alerts = False
		# 关闭屏幕更新,可加快宏的执行速度
		self.app.screen_updating = False
		self.filePath = file_path
		self.wb = self.app.books.open(file_path)

	def get_sheet(self, sheet_name):
		return Sheet(self.wb, sheet_name)

	def delete_sheet(self, sheet_name):
		self.wb.sheets(sheet_name).delete()
		print('删除表单:' + sheet_name)

	def save(self):
		self.wb.save()
		self.wb.close()
		self.quit_app()

	def quit_app(self):
		self.app.quit()
