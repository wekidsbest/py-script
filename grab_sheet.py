# coding=utf-8
import sys
import openpyxl  # 导入处理excel的库

sourceFile = './resources/source/source.xlsx'
sourceSheetList = ['期货', '现货']
targetFile = './resources/target/target.xlsx'


def loadExcel():
    return openpyxl.load_workbook(sourceFile)


def run(wb):
    # 循环来读取每一个sheet
    for sheet in wb.sheetnames:
        if sheet not in sourceSheetList:
            ws = wb[sheet]
            wb.remove(ws)
    wb.save(targetFile)


if __name__ == '__main__':
    wb = loadExcel()
    run(wb)
    wb.close()
    print("代码执行完毕！")
